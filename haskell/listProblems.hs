main :: IO()
main = print "Problems and solutions for lists"

-- Find last problem of a list
lastElem :: [a] -> Maybe a
lastElem []     = Nothing
lastElem [a]    = Just a
lastElem (_:xs) = lastElem xs

-- Find the K'th element of a list.
elemAt :: (Eq a, Num a) => a -> [a] -> a
elemAt _ []       = error "Index too large."
elemAt 0 (x:_)    = x
elemAt idx (_:xs) = elemAt (idx-1) xs

-- Find out whether a list is a palindrome.
-- A palindrome can be read forward or backward;
-- e.g. (x a m a x).
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome []  = True
isPalindrome [_] = True
isPalindrome (x:xs)
    | x == last xs = isPalindrome (init xs)
    | otherwise = False


-- Eliminate consecutive duplicates of list elements.
compress :: (Eq a) => [a] -> [a]
compress []  = []
compress [a] = [a]
compress (x:xs)
    | x == head xs = compress xs
    | otherwise = x:compress xs

-- Pack consecutive duplicates of list elements into sublists.
-- If a list contains repeated elements they should be placed
-- in separate sublists.
pack :: (Eq a) => [a] -> [[a]]
pack []  = []
pack [a] = [[a]]
pack (x:xs)  = packed:pack remaining
  where
    packed = takeWhile (==x) (x:xs)
    remaining = dropWhile (==x) (x:xs)


--  Run-length encoding of a list.
-- Use the result of problem P09 to implement
-- the so-called run-length encoding data compression method.
--  Consecutive duplicates of elements are encoded as
-- lists (N E) where N is the number of duplicates of the element E.
encode :: (Eq a) => [a] -> [(a, Int)]
encode []  = []
encode [a] = [(a, 1)]
encode (x:xs) = (x, length packed): encode remaining
  where
    packed = takeWhile (==x) (x:xs)
    remaining = dropWhile (==x) (x:xs)

doubleMe :: Num a => a -> a
doubleMe x = 2 * x -- Double the number given in input

main :: IO ()
main = print $ doubleMe 4

doubleSmallNumber :: (Num a, Ord a) => a -> a
doubleSmallNumber x = if x > 100
                        then x
                        else doubleMe x

factorial 0 = 1
factorial n = n * factorial ( n - 1)

bmiTell bmi
    | bmi < 18.5 = "You're underweight, you emo, you!"
    | bmi < 25 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | bmi < 30 = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"

bmiTell2 weight height
    | weight / height ^ 2 < 18.5 = "You're underweight, you emo, you!"
    | weight / height ^ 2 < 25 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | weight / height ^ 2 < 30 = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"

bmiTell3 weight height
    | bmi < 18.5 = "You're underweight, you emo, you!"
    | bmi < 25 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | bmi < 30 = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"
    where bmi = weight / height ^ 2

bmiTell4 weight height
    | bmi < 18.5 = "You're underweight, you emo, you!"
    | bmi < 25 = "You're supposedly normal. Pffft, I bet you're ugly!"
    | bmi < 30 = "You're fat! Lose some weight, fatty!"
    | otherwise = "You're a whale, congratulations!"
    where bmi = weight / height ^ 2
          (skinny, normal, fat) = (18.5, 25, 30)

describeList x = "This list is " ++ case x of []  -> "Empty"
                                              [a] -> "Singleton"
                                              x   -> "longer."
abc []  = "Empty list."
abc [1] = "Singleton list with 1."
abc [a] = "Singleton list."
abc x   = "Large list."

r = 12
area x = pi * r ^ 2

maximum' :: (Ord a) => [a] -> a
maximum' [] = error "Maximum of empty list"
maximum' [a] = a
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum xs

maximum2 :: (Ord a) => [a] -> a
maximum2 []     = error "Maximum of empty list"
maximum2 [a]    = a
maximum2 (x:xs) = max x $ maximum2 xs


replicate' :: (Num i, Ord i) => i -> a -> [a]
replicate' count number
    | count <= 0 = []
    | otherwise = number:replicate' (count -1) number

replicate2 :: (Num i, Ord i) => i -> a -> [a]
replicate2 0 _          = []
replicate2 count number = number:replicate2 (count - 1) number


take' :: (Num i, Ord i) => i -> [a] -> [a]
take' _ [] = []
take' n (x:xs)
    | n <= 0 = []
    | otherwise = x:take' (n-1) xs

reverse' :: (Ord i) => [i] -> [i]
reverse' []     = []
reverse' (x:xs) = reverse' xs ++ [x]

reverse2 :: (Ord i) => [i] -> [i]
reverse2 []   = []
reverse2 list = last list:reverse (init list)

length' :: Num a => [a] -> a
length' []     = 0
length' (x:xs) = 1 + length' xs

sum' :: Num a => [a] -> a
sum' []     = 0
sum' (x:xs) = x + sum' xs

fib :: Int -> Integer
fib n
    | n <= 1 = 1
fib n = fib (n-1) + fib (n-2)

memoized_fib :: Int -> Integer
memoized_fib = (map fib [0 ..] !!)
   where fib 0 = 0
         fib 1 = 1
         fib n = memoized_fib (n-2) + memoized_fib (n-1)

isUpperCase = (`elem` ['A' .. 'Z'])

applyTwice :: (a -> a) -> a -> a
applyTwice f n = f $ f n

-- Add 1 twice to 10
twelve = applyTwice (+1) 10

-- prepend 3 twice to [1,2]
append3 = applyTwice (3:) [1,2]

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _          = []
zipWith' _ _ []          = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

flip' :: (a -> b -> c) -> (b -> a -> c)
flip' f = g
  where
    g x y = f y x

-- addThree :: Num a => t2 -> t1 -> t -> a -> a -> a -> a
-- addThree x y z = \x -> \y -> \z -> x + y + z

-- fib 0 = 0
-- fib 1 = 1
-- fib n = fib (n-1) + fib (n-2)

insert :: (Ord a) => a -> [a] -> [a]
insert a [] = [a]
insert a (x:xs)
    | a >= x = x:insert a xs
    | otherwise = a:x:xs

insertionSort :: (Ord a) => [a] -> [a]
insertionSort []  = []
insertionSort [a] = [a]
insertionSort (x:xs) = insert x (insertionSort xs)

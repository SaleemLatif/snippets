
data Bool2 = True | False

main :: IO()
main = print "Hello There!"


data Point = Point Float Float deriving (Show)
data Shape = Circle Point Float | Rectangle Point Point deriving (Show)

data Car = Car {
  company :: String,
  model   :: String,
  year    :: Int
  } deriving (Show)

data List a = Empty | a `Cons` (List a) deriving (Show, Read, Eq, Ord)

lengthL :: List a -> Integer
lengthL Empty         = 0
lengthL (_ `Cons` xs) = 1 + lengthL xs



def hash_function(key, m):
    return hash(key) % m


class HashItem(object):
    key = None
    value = None

    def __init__(self, key, value):
        self.key = key
        self.value = value


class HashTable(object):
    m = 50
    __array__ = [None]*m

    def insert(self, key, item):
        index = hash_function(key, self.m)
        if self.__array__[index] is None:
            self.__array__[index] = []
        self.__array__[index].append(HashItem(key, item))

    def retrieve(self, key):
        index = hash_function(key, self.m)
        if self.__array__[index] is None:
            raise KeyError("{} not presesnt.".format(key))

        for item in self.__array__[index]:
            if item.key == key:
                return item.value

        raise KeyError("{} not present.".format(key))

    def delete(self, key):
        index = hash_function(key, self.m)
        if self.__array__[index] is None:
            raise KeyError("{} not presesnt.".format(key))

        for count, item in enumerate(self.__array__[index]):
            if item.key == key:
                del self.__array__[index][count]
                return

        raise KeyError("{} not present.".format(key))

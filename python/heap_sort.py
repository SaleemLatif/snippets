
def heapify(array, length, i):
    """
    Recursively Heapify given element in the given array.
    """
    l = 2*i + 1
    r = 2*i + 2
    greatest = i

    if l < length and array[l] > array[i]:
        greatest = l
    if r < length and array[r] > array[greatest]:
        greatest = r

    if greatest != i:
        array[greatest], array[i] = array[i], array[greatest]
        heapify(array, length, greatest)


def heap_sort(array):
    """
    Sort given array via heap sort.

    :param array: Un-sorted array
    :return: sorted array
    """
    length = len(array)
    for n in range(length, -1, -1):
        heapify(array, length, n)

    for n in range(length-1, 0, -1):
        array[n], array[0] = array[0], array[n]
        heapify(array, n, 0)

if __name__ == "__main__":
    array = [5, 2, 1, 5, 7, 9, 4, 0, 1, 3, 10, 8, 6]
    print "Before Sort:", array

    heap_sort(array)
    print "After Sort:", array



def count_sort(list):
    counter_list = [0]*len(list)

    for item in list:
        counter_list[item] += 1

    sorted_list = []
    for index, item in enumerate(counter_list):
        sorted_list.extend([index]*item)

    return sorted_list


if __name__ == "__main__":
    array = [5, 2, 1, 5, 7, 9, 4, 0, 1, 3, 10, 8, 6]
    print "Before Sort:", array

    array = count_sort(array)
    print "After Sort:", array

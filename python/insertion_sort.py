
def insertion_sort(array):
    for i in range(1, len(array)):
        j = i - 1
        key = array[i]

        while j >= 0 and array[j] > key:
            array[j + 1] = array[j]
            j -= 1
        array[j + 1] = key

if __name__ == "__main__":
    array = [5, 2, 1, 5, 7, 9, 4, 0, 1, 3, 10, 8, 6]
    print "Before Sort:", array

    insertion_sort(array)
    print "After Sort:", array

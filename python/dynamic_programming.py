# encoding: utf-8

"""
Examples of dynamic programming.
"""

INFINITY = float("inf")
NO_COIN = ""


# this one just returns number of coins
def sum_of_coins_v1(coins, _sum):
    """
    Problem Statement:
        Given a list of N coins, their values (V1, V2, ... , VN),
        and the total sum S. Find the minimum number of coins the sum of which is
        S (we can use as many coins of one type as we want), or report that it’s
        not possible to select coins in such a way that they sum up to S.
    """
    # dict has "coin: number of coins" as "key: value" pairs.
    memo = {}

    def DP(coins, _sum):
        if _sum == 0:
            return 0
        if _sum < 0:
            return INFINITY

        coin_count = INFINITY

        if _sum in memo:
            return memo[_sum]

        for coin, value in coins:
            result = 1 + DP(coins, _sum-value)

            if result < coin_count:
                coin_count = result

        memo[_sum] = coin_count
        return coin_count

    result = DP(coins, _sum)

    print result


# This one returns name of the coins along-with the number.
def sum_of_coins_v2(coins, _sum):
    """
    Problem Statement:
        Given a list of N coins, their values (V1, V2, ... , VN),
        and the total sum S. Find the minimum number of coins the sum of which is
        S (we can use as many coins of one type as we want), or report that it’s
        not possible to select coins in such a way that they sum up to S.
    """
    # dict has "coin: number of coins" as "key: value" pairs.
    memo = {}

    def DP(coins, _sum):
        if _sum == 0:
            return 0, {}
        if _sum < 0:
            return INFINITY, {}

        coin_count = INFINITY
        resulting_coins = {}

        if _sum in memo:
            return memo[_sum]

        for coin, value in coins:
            count, tmp = DP(coins, _sum-value)
            count = count + 1

            if count < coin_count:
                coin_count = count
                resulting_coins = dict(tmp, **{coin: tmp.get(coin, 0) + 1})

        memo[_sum] = coin_count, resulting_coins
        return coin_count, resulting_coins

    result = DP(coins, _sum)

    print result


def sum_of_coins_top_down(coins, _sum):
    """
    Problem Statement:
        Given a list of N coins, their values (V1, V2, ... , VN),
        and the total sum S. Find the minimum number of coins the sum of which is
        S (we can use as many coins of one type as we want), or report that it’s
        not possible to select coins in such a way that they sum up to S.
    """
    memo = {0: (0, 0, {})}

    for current in range(1, _sum+1):
        coin_count = INFINITY
        resulting_coins = {}
        current_sum = -INFINITY

        for key, item in memo.copy().iteritems():
            for coin, value in coins:
                count, previous_sum, tmp = item
                count = count + 1

                if count < coin_count and current == previous_sum + value:
                    coin_count = count
                    current_sum = previous_sum + value
                    resulting_coins = dict(tmp, **{coin: tmp.get(coin, 0) + 1})

            memo[current] = coin_count, current_sum, resulting_coins

    print memo[_sum]


def fib_rec(n):
    if n <= 2:
        return 1
    return fib_rec(n-1) + fib_rec(n-2)

fib_dp_bu_memo = {}


def fib_dp_td(n):
    if n <= 2:
        return 1

    if n in fib_dp_bu_memo:
        return fib_dp_bu_memo[n]
    result = fib_dp_td(n-1) + fib_dp_td(n-2)

    fib_dp_bu_memo[n] = result

    return result


def fib_dp_bu(n):
    nums = [1, 1]

    for number in range(2, n):
        nums.append(nums[number-1] + nums[number-2])
    return nums[n-1]


prices = [0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30]
cache = {}


def cut_roads_td(length):
    if length <= 0:
        return 0

    if length in cache:
        return cache[length]

    profit = -INFINITY
    for i in range(1, length+1):
        tmp = prices[i] + cut_roads_td(length-i)
        if profit < tmp:
            profit = tmp

    cache[length] = profit

    return profit


def cut_roads_bu(length):
    result = {0: 0}

    for j in range(1, length+1):
        profit = -INFINITY

        for i in range(1, j+1):
            tmp = prices[i] + result[j-i]

            if profit < tmp:
                profit = tmp

        result[j] = profit

    return result


if __name__ == "__main__":
    required_sum = 37
    coins = [("one", 1), ("two", 2), ("five", 5), ("ten", 10)]

    print "Sum: ", required_sum
    print "Coins: ", coins
    print "Version 1: "
    sum_of_coins_v1(coins, required_sum)
    print "Version 2: "
    sum_of_coins_v2(coins, required_sum)
    print "Version 3 Top Down: "
    sum_of_coins_top_down(coins, required_sum)

    print "Fibonacci"
    n = 12
    print fib_rec(n)
    print fib_dp_bu(n)
    print fib_dp_td(n)

    print "Cut Roads maximizing profit"
    n = 5
    print cut_roads_td(n)
    print cut_roads_bu(n)

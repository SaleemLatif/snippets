class BST(object):
    tree = None

    def __init__(self, tree=None):
        self.tree = tree

    def insert(self, key):
            self.tree = BST.insert_node(tree=self.tree, key=key, parent=None)

    @staticmethod
    def insert_node(tree, key, parent):
        if tree is None:
            return BSTNode(key, parent)
        if key > tree.key:
            tree.right = BST.insert_node(tree.right, key, tree)
        else:
            tree.left = BST.insert_node(tree.left, key, tree)

        return tree

    def extract_min(self):
        tree = self.tree
        if not tree:
            return
        while tree.left:
            tree = tree.left
        return tree.key

    def extract_max(self):
        tree = self.tree
        if not tree:
            return
        while tree.right:
            tree = tree.right
        return tree.key

    def get_sorted_array(self):
        array = []

        def _(key):
            array.append(key)

        self.traverse_in_order(self.tree, _)

        return array

    @classmethod
    def sort(cls, array):
        bst = cls.build_from_array(array)
        return bst.get_sorted_array()


    @classmethod
    def build_from_array(cls, array):
        bst = BST()
        for item in array:
            bst.insert(item)
        return bst

    @staticmethod
    def traverse_in_order(tree, func):
        if tree:
            BST.traverse_in_order(tree.left, func)
            func(tree.key)
            BST.traverse_in_order(tree.right, func)

    @staticmethod
    def traverse_pre_order(tree):
        if tree:
            print tree.key
            BST.traverse_pre_order(tree.left)
            BST.traverse_pre_order(tree.right)

    @staticmethod
    def traverse_post_order(tree):
        if tree:
            BST.traverse_post_order(tree.left)
            BST.traverse_post_order(tree.right)
            print tree.key


class BSTNode(object):
    parent = None
    left = None
    right = None

    key = None

    def __init__(self, key, parent=None, left=None, right=None):
        self.key = key
        self.parent = parent
        self.left = left
        self.right = right


def is_bst_balanced(bst):
    if not bst:
        return True

    # Tree is not balanced if it has a right child but does not have a left child and vice versa
    if bst.left and not bst.right:
        return False
    if bst.right and not bst.left:
        return False

    is_left_balanced = is_bst_balanced(bst.left)
    is_right_balanced = is_bst_balanced(bst.right)

    return any(not is_left_balanced, not is_right_balanced)

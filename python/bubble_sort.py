
def bubble_sort(array):
    for i in range(1, len(array)):
        for j in range(i, 0, -1):
            if array[j] < array[j-1]:
                array[j], array[j-1] = array[j-1], array[j]

if __name__ == "__main__":
    array = [5, 2, 1, 5, 7, 9, 4, 0, 1, 3, 10, 8, 6]
    print "Before Sort:", array

    bubble_sort(array)
    print "After Sort:", array

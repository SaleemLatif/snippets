import random
import timeit
import copy

from bubble_sort import bubble_sort
from insertion_sort import insertion_sort
from heap_sort import heap_sort
from merge_sort import merge_sort
from BST import BST
from count_sort import count_sort
from bucket_sort import bucket_sort

array = [random.randint(0, 5000) for r in xrange(10000)]
array1 = copy.deepcopy(array)
array2 = copy.deepcopy(array)
array3 = copy.deepcopy(array)
array4 = copy.deepcopy(array)
array5 = copy.deepcopy(array)
array6 = copy.deepcopy(array)
array7 = copy.deepcopy(array)

if __name__ == '__main__':

    bubble_sort_timer = timeit.Timer("bubble_sort(arr)", setup="from __main__ import bubble_sort, array1 as arr")
    insertion_sort_timer = timeit.Timer("insertion_sort(arr)", setup="from __main__ import insertion_sort, array2 as arr")
    heap_sort_timer = timeit.Timer("heap_sort(arr)", setup="from __main__ import heap_sort, array3 as arr")
    merge_sort_timer = timeit.Timer("merge_sort(arr)", setup="from __main__ import merge_sort, array4 as arr")
    bst_sort_timer = timeit.Timer("BST.sort(arr)", setup="from __main__ import BST, array as arr")
    count_sort_timer = timeit.Timer("count_sort(arr)", setup="from __main__ import count_sort, array5 as arr")
    bucket_sort_timer = timeit.Timer("bucket_sort(arr)", setup="from __main__ import bucket_sort, array6 as arr")
    python_sort_timer = timeit.Timer("sorted(arr)", setup="from __main__ import array6 as arr")

    print "Array Size: {}".format(len(array))
    print "Comparison Started"

    print "Bubble Sort Execution Time: ", bubble_sort_timer.timeit(1)
    print "Insertion Sort Execution Time: ", insertion_sort_timer.timeit(1)
    print "Heap Sort Execution Time: ", heap_sort_timer.timeit(1)
    print "Merge Sort Execution Time: ", merge_sort_timer.timeit(1)
    print "BST Sort Execution Time: ", bst_sort_timer.timeit(1)
    print "Count Sort Execution Time: ", count_sort_timer.timeit(1)
    print "Bucket Sort Execution Time: ", bucket_sort_timer.timeit(1)
    print "Python Sort Execution Time: ", python_sort_timer.timeit(1)

    print "Comparison Ended"

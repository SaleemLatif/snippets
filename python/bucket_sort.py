

def bucket_sort(list):
    buckets = [[] for x in xrange(0, len(list))]

    for item in list:
        buckets[item].append(item)

    sorted_list = []
    for item in buckets:
        sorted_list.extend(item)

    return sorted_list


if __name__ == "__main__":
    array = [5, 2, 1, 5, 7, 9, 4, 0, 1, 3, 10, 8, 6]
    print "Before Sort:", array

    array = bucket_sort(array)
    print "After Sort:", array


def merge_sort(array):
    mid = len(array) / 2
    if len(array) == 1:
        return array
    else:
        l = merge_sort(array[:mid])
        r = merge_sort(array[mid:])
        return merge(l, r)


def merge(first, second):
    """
    Merge given arrays so that resulted array is sorted

    :param first: First sorted array
    :param second: Second Sorted array
    :return:
    """
    merged = []
    j = 0
    i = 0

    while i < len(first) and j < len(second):
        if first[i] <= second[j]:
            merged.append(first[i])
            i += 1
        else:
            merged.append(second[j])
            j += 1

    while i < len(first):
        merged.append(first[i])
        i += 1

    while j < len(second):
        merged.append(second[j])
        j += 1
    return merged

if __name__ == "__main__":
    array = [5, 2, 1, 5, 7, 9, 4, 0, 1, 3, 10, 8, 6]
    print "Before Sort:", array

    array = merge_sort(array)
    print "After Sort:", array


def map_by_reduce(func, iterator):
    """
    This function implements map function using reduce.

    Arguments:
        func: function that takes an item from iterator operates on it and returns something.
        iterator: an iterator that returns items to apply map upon.
    """
    return reduce(lambda memo, a: memo + [func(a)], iterator, [])


def filter_by_reduce(func, iterator):
    """
    This function implements map function using reduce.

    Arguments:
        func: function that takes an item from iterator operates on it and returns something.
        iterator: an iterator that returns items to apply map upon.
    """
    return reduce(lambda memo, a: memo + [a] if func(a) else memo, iterator, [])


def take_while(func, iterator):
    class __(object):
        stop = False

    def _(memo, item):
        if not __.stop:
            if func(item):
                return memo + [item]
            __.stop = True
        return memo

    return reduce(_, iterator, [])


def sum_rec(n):
    if n <= 0:
        return 0
    return n + sum_rec(n-1)


def sum_fn(n):
    return reduce(lambda memo, item: item + memo, xrange(n+1), 0)


def factorial_rec(n):
    if n <= 0:
        return 1
    return n * factorial_rec(n-1)


def factorial_fn(n):
    return reduce(lambda memo, item: item*memo, xrange(n+1), 1)


def fib_rec(n):
    if n <= 2:
        return 1
    return fib_rec(n-1) + fib_rec(n-2)


def fib_fn(n):
    return reduce(lambda memo, item: (memo[1], sum(memo)), xrange(1, n), (1, 1))[0]


def fib_n(n):
    current = 1
    previous = 0
    count = 0
    while count < n:
        count += 1
        yield current
        previous, current = current, previous + current


def reverse_rec(list):
    if len(list) == 0:
        return []
    return reverse_rec(list[1:]) + list[:1]


def reverse_fn(list):
    return reduce(lambda memo, item: [item] + memo, list, [])


def length_rec(list):
    if not list:
        return 0
    return 1 + length_rec(list[1:])


def length_fn(list):
    return reduce(lambda memo, item: 1 + memo, list, 0)


def de_dupe_iter(list):
    seen = set()
    for item in list:
        if item not in seen:
            yield item
            seen.add(item)


def delete_rec(list, item):
    if not list:
        return []
    return ([] if item == list[0] else list[:1]) + delete_rec(list[1:], item)


def de_dupe_rec(list):
    if not list:
        return []
    return list[:1] + de_dupe_rec(delete_rec(list[1:], list[0]))


def delete_fn(list, item):
    return reduce(lambda memo, x: ([x] if x != item else []) + memo, list, [])


def de_dupe_fn(list):
    return reduce(lambda memo, item: [item] + delete_fn(memo, item), list, [])


def counter_iter(list):
    tmp = {}
    for item in list:
        if item in tmp:
            tmp[item] += 1
        else:
            tmp[item] = 1
    return tmp


def counter_rec(list):
    if not list:
        return {}
    result = counter_rec(list[1:])
    return dict(result, **{list[0]: 1 + (result[list[0]] if list[0] in result else 0)})


def counter_fn(list):
    return reduce(lambda memo, item: dict(memo, **{item: 1 + (memo[item] if item in memo else 0)}), list, {})


def mod_iter(numerator, denominator):
    while numerator > denominator:
        numerator -= denominator
    return numerator


def mod_rec(numerator, denominator):
    if numerator < denominator:
        return numerator
    return mod_rec(numerator-denominator, denominator)

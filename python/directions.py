"""
Consider three two-dimensional points a, b, and c. If we look at the angle formed by the
line segment from a to b and the line segment from b to c, it either turns left,
turns right, or forms a straight line.

Write a function that calculates the turn made by three 2D points and returns a Direction.
Define a function that takes a list of 2D points and computes the direction of each successive triple.
Given a list of points [a,b,c,d,e], it should begin by computing the turn made by [a,b,c],
then the turn made by [b,c,d], then [c,d,e]. Your function should return a list of Direction.
"""

LEFT = -1
RIGHT = 1
STRAIGHT = 0


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Line(object):
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

    @property
    def slope(self):
        """
        Commonly referred as 'm' in line slope equation (y = mx + b).
        """
        return (self.p2.y - self.p1.y)/(self.p2.x - self.p1.x)

    @property
    def offset(self):
        """
        Commonly referred as 'b' in line slope equation (y = mx + b).
        """
        return self.p1.y - self.slope*self.p1.x

    def get_y(self, x):
        return self.slope * x + self.offset

    def get_x(self, y):
        return (y - self.offset)/self.slope

    def is_left(self, point):
        if (self.p2.x - self.p1.x) == 0:
            return point.x < 0

        if self.slope >= 0:
            return point.y > self.get_y(point.x)

        if self.slope < 0:
            return point.y < self.get_y(point.x)

    def is_right(self, point):
        if (self.p2.x - self.p1.x) == 0:
            return point.x > 0

        if self.slope >= 0:
            return point.y < self.get_y(point.x)

        if self.slope < 0:
            return point.y > self.get_y(point.x)


def directions(points):
    counter = 0
    directions_list = []
    while len(points[counter:counter+3]) == 3:
        directions_list.append(turn(points[counter:counter+3]))
        counter += 1

    return directions_list


def turn(points):
    """
    Given a list of three points (a, b, c), return the direction of the turn made between (a, b) and (a, c)

    Arguments:
        points: (tuple) - tuple of length 3.

    Returns:
         Direction of the turn.
    """
    line = Line(*points[:2])
    if line.is_left(points[2]):
        return LEFT
    if line.is_right(points[2]):
        return RIGHT
    return STRAIGHT

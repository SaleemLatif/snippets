from pprint import pprint
from Queue import Queue

adjacency_lists = {
    "A": ['B', 'C', 'D'],
    "B": ['A', 'G'],
    "C": ['A', 'F', 'E'],
    "D": ['A', 'E'],
    "E": ['C', 'D', 'I'],
    "F": ['G', 'H', 'C'],
    "G": ['F', 'B'],
    "H": ['F', 'I'],
    "I": ['E', 'H'],
}


def explore_graph_bfs(graph, start='A'):
    visited = {start: 0}
    parent = {start: None}

    list = [start]
    while len(list) > 0:
        item = list[0]
        list = list[1:]

        for node in graph[item]:
            if node not in visited:
                visited[node] = visited[item] + 1
                list.append(node)
                parent[node] = item

    return parent, visited


def shortest_path(graph, start, end):
    parent, visited = explore_graph_bfs(graph, start)

    print "Path Length: ", visited[end]

    def print_path(parents, item):
        if parents[item]:
            return "{} -> {}".format(item, print_path(parents, parents[item]))
        return "{}".format(item)

    print print_path(parent, end)

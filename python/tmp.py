"""
Temporary functions
"""


def rotate(array, count):
    """
    Rotate an array by given count in.

    Arguments:
        array (list): A list of numbers.
        count (int): An integer telling the amount of rotation.

    Returns:
         (list): Rotated array.
    """
    count = (count % len(array)) if len(array) else 0
    return array[count:] + array[:count]

PERMUTATIONS = []


def permutations(array):
    """
    Find permutations of given list.
    """
    if not array:
        return array

    PERMUTATIONS.append(array)
    elem = array[0]
    r = array[1:]
    for a, b in array:
        for idx, _ in enumerate(r, start=1):
            PERMUTATIONS.append(
                r[:idx] + [elem] + r[idx:]
            )


if __name__ == "__main__":
    array = [1,2,3,4]
    p = permutations(array)
    print p
